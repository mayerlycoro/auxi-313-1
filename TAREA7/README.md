# ***PRÁCTICA 7***  📚  👩🏻‍💻

**-DATOS PERSONALES**
| **NOMBRE  🌻** | **MATERIA  💻**    | **AUXILIAR  🐱**                |
| :-------- | :------- | :------------------------- |
| *Mayerly Coro Canaviri* | *Diseño y Programacion* | *Sergio Apaza*|

```http
```
***TAREAS A COMPLETAR*** 🤓


- [X]  ☝️ Entrega en formato digital en repositorio: La práctica debe ser entregada en formato digital en un repositorio específico.
- [X] ☝️ Sacar captura a las los primeros 5 nivel y 10 ultimos niveles. 
- [X]  ☝️ Capturas de pantalla claras: Las capturas de pantalla deben ser de la pantalla completa y deben mostrar claramente la hora.
- [X]  ☝️Presentación del video: El video puede ser subido a algún servidor y presentado en un archivo .md.


```http
```
***OBJETIVO***  ⚠  📚

+ ✔️ El objetivo de la tarea es poder desarrollar habilidades prácticas en el uso de la terminal tanto en entornos Windows como Linux.
+ ✔️ Aprender a realizar tareas básicas como la creación de directorios, la navegación entre carpetas y el uso de comandos básicos de la terminal.
+ ✔️ La tarea busca fomentar la presentación ordenada de resultados, incluyendo capturas de pantalla y la creación de un video explicativo. 
+ ✔️ Al especificar el nombre del archivo de entrega, se promueve la organización y la claridad en la presentación de los trabajos. 

```http

```
## ***PREGUNTA 1*** 👩🏻‍💻 🌻
- [X]    📷 Nos piden la elavoracion de un video explicando la creacion de una carpeta y tambien el poder explicar de como podemos cambiar de directorio una carpeta  📂. 

+ Nota: para ello nos convertimos en unos grandes youtubers 😎 dejando los miedos atras  😰 y poder ser claros y consisos con la explicacion , para que el AUXI 😸 pueda entender☝️. 
+ Eh aqui el video➡️   [ 📷 ](https://drive.google.com/file/d/1Y3hOKOe4Mj6v5Ohs77JeOU85wI-7ZgwL/view?usp=drive_link). (clik en la camarita)



```http

```
 

## ***PREGUNTA 2*** 👩🏻‍💻 🌻

- [X]    📘Adjuntar capturas de pantalla del mismo proceso, tres veces más.
+ Entonces ahora mostraremos las capturas de pantallas de las carpetitas creadas  👇

***Captura 1  🔍***

![Captura de pantalla 2024-04-23 200147](https://github.com/mayerlyCoro/imagen/assets/164127557/f4dc9330-c762-4663-889a-e92ea08f595c)

***Captura 2  🔍***


![Captura de pantalla 2024-04-23 200044](https://github.com/mayerlyCoro/imagen/assets/164127557/406b0c88-1a10-4aba-b995-5c84987fc80a)


***Captura 3  🔍***



![Captura de pantalla 2024-04-23 195925](https://github.com/mayerlyCoro/imagen/assets/164127557/6aa1b5df-d745-4741-8c9c-155c4bc32669)



```http

```
 

## ***PREGUNTA 3*** 👩🏻‍💻 🌻

- [x]   Que comandos se utilizó en las anteriores preguntas.

***LOS COMANDOS UNTILIZADOS***  😌

▶️MD : Este comando se utiliza para crear un nuevo directorio o carpeta en el sistema de archivos. 

▶️CD : Este comando se utiliza para cambiar el directorio actual en el que te encuentras en la terminal o símbolo del sistema. Con "CD" se puede navegar a través de diferentes carpetas en el sistema de archivos. 

▶️DIR : Este comando se utiliza para listar el contenido de un directorio en la terminal de Windows. 
CD.. : Este comando se utiliza para cambiar al directorio padre o superior del directorio actual.

▶️CD C:\ : En Windows, este comando se utiliza para cambiar al directorio raíz del disco C:. Es similar a cambiar al directorio principal, pero específicamente para el disco C:. Esto se logra escribiendo CD C:\ y presionando Enter.

```http

```
 

## ***PREGUNTA 4*** 👩🏻‍💻 🌻
- [x] Explique 5 comandos para ser utilizados en la terminal y su forma de usarse en Windows (Terminal o Powershell) y en Linux (bash)


***COMANDOS EN LINUX*** 💻 🐧

✅***DIR (Directory)***:  este comando te muestra una lista de archivos y carpetas en el directorio actual. En la Terminal o PowerShell de Windows, escribes DIR y presionas Enter.

✅***DEL (Delete)***: Este comando se utiliza para eliminar archivos. Puedes usarlo en la Terminal o PowerShell de Windows escribiendo DEL seguido del nombre del archivo que deseas eliminar.  

✅***CLS (Clear Screen)***: Con este comando puedemos limpiar la pantalla de la Terminal o PowerShell, eliminando el texto anterior. Simplemente escribes CLS y presionas Enter.

✅***COPY (Copy)***: Este comando  permite copiar archivos de un lugar a otro. En la Terminal o PowerShell de Windows, escribes COPY seguido del nombre del archivo que deseas copiar y la ubicación a la que deseas copiarlo. 

✅***MOVE (Move)***: Con este comando puedes mover archivos de una ubicación a otra. En la Terminal o PowerShell de Windows, escribes MOVE seguido del nombre del archivo que deseas mover y la ubicación a la que deseamos moverlo. Por ejemplo: MOVE archivo.txt C:\CarpetaDestino

---------------------------------------------------------------------

***COMANDOS EN WINDOWS*** 💻

✅***mkdir (Make Directory)***: Este comando se usa para crear una nueva carpeta o directorio. En Linux, en la terminal (usando Bash), escribes mkdir seguido del nombre de la carpeta que deseas crear. 

✅***cd (Change Directory)***: Con este comando puedemos movernos entre carpetas. En Linux, en la terminal, escribes cd seguido del nombre de la carpeta a la que quieremos ir. 

✅***ls (List)***: Este comando nos muestra una lista de archivos y carpetas en el directorio actual. En Linux, en la terminal, solo necesitamos escribir ls y presionar Enter.

✅***cd .. (Change to Parent Directory)***: Este comando nos lleva de vuelta a la carpeta anterior. En Linux, en la terminal, solo escribes cd .. y presionamos Enter.

✅***cd / (Change to Root Directory)***: Este comando nos lleva directamente al directorio raíz del sistema de archivos. En Linux, en la terminal, escribimos cd / y presionamos Enter.



****TAREA COMPLETADA**** 🎉