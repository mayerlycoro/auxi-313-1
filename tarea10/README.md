# ***PRÁCTICA 10***  📚  👩🏻‍💻

**-DATOS PERSONALES**
| **NOMBRE  🌻** | **MATERIA  💻**    | **AUXILIAR  🐱**                |
| :-------- | :------- | :------------------------- |
| *Mayerly Coro Canaviri* | *Diseño y Programacion* | *Sergio Apaza*|

```http
```
***TAREAS A COMPLETAR*** 🤓


- [X]  ☝️  Crear un nuevo proyecto de Gitlab que sirva de repositorio para las tareas de aquí en adelante; el repositorio deberá ser nombrado “React-with-<NombreApellido>” (en “NombreApellido” poner sus nombres y apellidos).
+ PARA EL NUEVO REPOSITORIO:
1 . Buscar dos TEMPLATES que puedan utilizarse con Figma.
PD: Solo será utilizado un template, el que tengan dos a la mano es en caso de que otro estudiante haya registrado ese template para su uso.

2 . Se deberá crear un proyecto en React usando Next con el nombre “frontendreact”.
- [X]  ☝️ PARA EL ANTIGUO REPOSITORIO (el que lleva por nombre (auxSIS313g1-…)):
1 . Para la práctica, subir un archivo .md como se indica en la parte de NOTA(S) 
(es decir, debe llevar el nombre “HW10_templateIdeas.md”).
Presentar lo siguiente:

• Link del template.

• Link del proyecto en Figma utilizando el template.

• Link del repositorio creado


```http

```
## ***PREGUNTA 1*** 👩🏻‍💻 🌻
- [X]     🌟 Crear un nuevo proyecto de Gitlab que sirva de repositorio para las tareas de aquí en adelante

+ REPOSITORIO  🙈

⚠️***Aqui el REPOSITORIO, presione la Lupita***     [  🔎 ](https://gitlab.com/programacion-ii2/react-with-mayerlycoro)


```http

```
 

## ***PREGUNTA 2*** 👩🏻‍💻 🌻

- [X]    📘Para el antiguo repositorio:  👐

 Presentar lo siguiente:

• Link del template.

• Link del proyecto en Figma utilizando el template.

• Link del repositorio creado.😎 😈  🤘

1. ⚠️***Aqui el Link del Primer template, presione la Lupita***     [  🔎 ](https://freebiesbug.com/figma-freebies/plant-ecommerce-template/)

2. ⚠️***Aqui el Link del proyecto en Figma utilizando el template., presione la Lupita***     [  🔎 ](https://www.figma.com/file/iGGRH9qgnpT1ApfiXdwWip/E-Commerce-Plant-Shop-Website-(Community)?type=design&node-id=0-1&mode=design&t=yJIeZ7FyzgdBlEXw-0)

3. ⚠️***Aqui el Link del Segunto template, presione la Lupita***     [  🔎 ](https://freebiesbug.com/figma-freebies/portfolio-figma/)

4. ⚠️***Aqui el Link del proyecto en Figma utilizando el template., presione la Lupita***     [  🔎 ](https://www.figma.com/file/CHyYoHbFW8VBWGaO0UO8OI/Arnau-Ros-%E2%80%A2-Portfolio-Template?type=design&node-id=0-1&mode=design&t=2Fo8JiJnwJaZzaBt-0)


Nota: el link del repositorio esta en la pregunta 1. 

```http

```
 




****TAREA COMPLETADA**** 🎉