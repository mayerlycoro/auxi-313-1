# PRÁCTICA


**1. Nombre:** Mayerly Belen Coro Canaviri

**2. Asignatura:** Diseño y programación

**3. fecha: :** 19/03/24

- [x]  La elavoracion de la práctica consiste en poner completar el nivel medio y dificil del juevo **flex box aventura** con la finalidad de poner conocer y aprender propiedades correctas para la elavoración de un buen código. 

![Screenshot of a comment on a GitHub issue showing an image, added in the Markdown, of an Octocat smiling and raising a tentacle.](https://codingfantasy.com/_next/image?url=%2FgamesAssets%2Fflexbox%2Flanding%2Fheroes.png&w=1200&q=100)

- [x]  la elavoración de la práctica constira en explicar cada nivel que pudo ser superada aclarando que el nivel medio y dificil son los mismos ejercicios con la diferencia que en el nivel dificil no hay nunguna ayuda como en el nivel medio. 

# ***Nivel 1***
 En el nivel ayudaremos a Arthur Utilizando el justificar-contenido propiedad la cual ayuda a Arthur en la manzana. siendo el codigo:
  **justify-content:center;**

# ***Nivel 2***
Ayudaremos a Arthur a que pueda llegar a su comida con: 
**justify-content:flex-end;**

# ***Nivel 3*** 
Marilyn, la amiga de Arthur. Y parece nos indican que ella esta un poco enferma. Utilizamos el justificar-contenido propiedad y asi llegaran a su comida. el codigo es **justify-content:space-between;**

# ***Nivel 4*** 
Utilizamos el justificar-contenidopropiedad y colocamos a los héroes en la comida.

# ***Nivel 5*** 
Utilizamos eljustificar-contenido propiedad y colocar a los héroes en la comida. con erl codigo: **justify-content:space-evenly**

![Captura de pantalla 2024-03-20 004433](https://github.com/mayerlyCoro/imagen/assets/164127557/1fec0976-9868-48bd-9163-7469dcd2a8a8)


# ***Nivel 6***  
Parecen los primeros enemigos. se hara el uso del codigo 
**align-items:center;** para colocar a nuestros héroes sobre los mosquitos para derrotarlos

# ***Nivel 7***
Derrotaremos nuevamente a los mosquitos donde usaremos el codiogo de **align-items:flex-end;** para lograr el objetivo

# ***Nivel 8***
Esta vez necesitaremos usar dos propiedades. aplicando el codigo de **align-items:center; 
justify-content:center;**

# ***Nivel 9***
Derrotaremos a los Trosl y para ello usaremos propiedades inplementada en el codigo **align-items:flex-end; 
justify-content:space-between;**

# ***Nivel 10***
Los tres heroes tendran que derrotar a los tres enemigos para ello implementaremos el codigo **flex-direction:column;**

# ***Nivel 11***
Plutus tiene un gran ejército. el objetivo es derrotar a los tres enemigos con el codigo **flex-direction: row-reverse;**

![Captura de pantalla 2024-03-20 004930](https://github.com/mayerlyCoro/imagen/assets/164127557/3de42f43-1e83-4065-a862-6c2c9829a36d)


# ***Nivel 12***
Derrotaremos a los dos gragones y al trol que quedan con la propiedad de 

**flex-direction: column; 
justify-content:flex-end;**

# ***Nivel 13*** 
Aquí está Plutus. esperemos que esté listo para esta pelea Para derrotarlo necesitas usar las propiedades correctas **flex-direction: row-reverse; 
justify-content:center; 
align-items:center;**

# ***Nivel 14*** 
Para  recuperar la salud del caballero necesitamos que coma su pedazo de carne para ello utilizaremos la propiedad de **orde:2;**

# ***Nivel 15***
Ayudamos a zelus a que puede combatir con la calabera con un ** align-self:center;**, cumpliendo el objetivo usando la propiedad de alinear. 

# ***Nivel 16*** 
En  este nivel derrotaremos al fantasma nuevamente ysando el aliniarse y orden de propiedades **align-self: flex-end ; order:1;**para poder cumplir el objetivo. el order:-1, nos permite que el caballero a que retroceda hacia el fantasma 

# ***Nivel 17*** 
En este nivel usaremos la direcciion flexible,justificar-contenido, aliniar elementos, con los codigos de: 

 **flex-direction:column-reverse;
 justify-content:flex-end;
 align-items:center;**

  compliendo el objetivo de combatir con el enemigo . 

# ***Nivel 18*** 
Para derrotar a zelus arthhur lo hara ebtonces usamos el alinear y el orden con el codigo de :

**align-self:center;
order:2;**

para dar el golpe final y cumplir el objetivo

# ***Nivel 19***  
En este nivel el objetivo es llevar las moneditas a cada cofre, con ello utilizaremos la envoltura flexible siendo el codigo . 
**flex-wrap:wrap;**

asi podremos recoger todas las monedas . 

# ***Nivel 20*** 
En este nivel de igual manera el objetivo es popder llevar las onedas a su cofre para ello alinearemois el contenido: 

 **align-content:center;** 
 
 para que Arthur se haga mas rico. 

# ***Nivel 21***
Robaron el oro y ahora pondremos clada moneda en su cofre utilizado el alinear contenido y el justificar-contenido.

 **align-content:flex-end;justify-content:center;** 

# ***Nivel 22***
En esta ocacion el objetivo es poder derrotar a los tres eneminos que tienen los caballeros Colocando a cada héroe contra su enemigo usando eldirección flexible, justificar-contenido y alinear elementos propiedades.

# ***Nivel 23***
Colocamos a cada héroe contra su enemigo y posicionamos las monedas en cofres usando el dirección flexible,justificar-contenido, envoltura flexibleyalinear contenidopropiedades. con el codigo: 

**flex-direction:column-reverse; 
justify-content:center; 
flex-wrap:wrap-reverse;
align-content:center;**

![Captura de pantalla 2024-03-20 004433](https://github.com/mayerlyCoro/imagen/assets/164127557/1fec0976-9868-48bd-9163-7469dcd2a8a8) 



# ***Nivel 24***
Arthur para derrivar al enemigo Utilizar elalinearse y orden propiedades para ayudarlos con el codigo.

**align-self:center;
order:3;**

![Captura de pantalla 2024-03-20 004624](https://github.com/mayerlyCoro/imagen/assets/164127557/510c049b-81b9-499f-bde7-0ed0e5be1a95)


- [x] Unas de tareas también fue subir las ultimas fotos del nivel medio y nivel dificil, esto para una verificacion de la elavoración del codigo. 


***Imagen, 21 nivel medio***
![21medio](https://github.com/mayerlyCoro/imagen/assets/164127557/e4a3fe47-6b11-4c6f-8bc1-f4fa487bafa3)






***Imagen, 22 nivel medio***
![22medio](https://github.com/mayerlyCoro/imagen/assets/164127557/54190262-7ab9-41a5-b3b0-79eb9e30873f)






***Imagen, 23 nivel medio***
![23medio](https://github.com/mayerlyCoro/imagen/assets/164127557/4c1ae4d1-6da3-42e0-9fc2-7c257e54fe3b)






***Imagen, 24 nivel medio***
![24medio](https://github.com/mayerlyCoro/imagen/assets/164127557/7cc324cb-fa25-4f30-8dc0-27ea668a9a0e)






***Imagen, 21 nivel dificil***
![21dificil](https://github.com/mayerlyCoro/imagen/assets/164127557/fc5df8e4-db6a-4a74-840a-1cbc6a1efba6)








***Imagen, 22 nivel dificil***
![22dificil](https://github.com/mayerlyCoro/imagen/assets/164127557/efe677b1-50a9-4810-afd0-62e4b2aca67d)







***Imagen, 23 nivel dificil***
![23dificil](https://github.com/mayerlyCoro/imagen/assets/164127557/7540267e-259c-4369-b07c-602f5467988c)








***Imagen, 24 nivel medio***
![24dificil](https://github.com/mayerlyCoro/imagen/assets/164127557/84f9e24b-9143-481d-8e51-d4b5dd8775a4)









