# ***PRÁCTICA 6***  📚  👩🏻‍💻

**-DATOS PERSONALES**
| **NOMBRE** | **MATERIA**    | **AUXILIAR**                |
| :-------- | :------- | :------------------------- |
| *Mayerly Coro Canaviri* | *Diseño y Programacion* | *Sergio Apaza*|

```http
```
***TAREAS A COMPLETAR*** 🤓


- [X]  Completar todos los niveles del juego CSS DINNER 
- [X]  Sacar captura a las los primeros 5 nivel y 10 ultimos niveles. 
- [X]  Sacar captura a la lista de niveles vencidos. 
- [X]  Presiona [AQUI](https://www.google.com/search?q=css+diner&oq=&gs_lcrp=EgZjaHJvbWUqCQgBECMYJxjqAjIJCAAQIxgnGOoCMgkIARAjGCcY6gIyCQgCECMYJxjqAjIJCAMQIxgnGOoCMgkIBBAjGCcY6gIyCQgFECMYJxjqAjIJCAYQIxgnGOoCMgkIBxAjGCcY6gIyGwgIEC4YQxjHARi0AhjqAhjIAxjRAxiABBiKBdIBCTE2NjdqMGoxNagCCbACAQ&sourceid=chrome&ie=UTF-8). para entrar a la pagina

![capturita](https://i.ytimg.com/vi/BaiOGOmK9Mk/maxresdefault.jpg)
```http
```
***OBJETIVO***  ⚠

+ Poner aprender mas sobre CSS y HTML expandiendose asi los conocimientos. 
+ Nota: Se dara una breve explicacion de cada nivel con ellos sus objetivos y su resulucion, para un mejor entendimiento se incluira imagenes y tambien para la verificacion del auxiliar
```http

```
## ***NIVEL 1*** 🧠

![1](https://github.com/mayerlyCoro/imagen/assets/164127557/4b2bd1cf-3d1b-471e-9228-094a21047a99)

##### **COMPROBANTE** ☝

**Objetivo**: debemos seleccionar los platos, seleecioanado todas las etiquetas, por que para seleccionar la etiqueta solo con el nombre de esas etiqurta 

## ***NIVEL 2*** 🧠

![2](https://github.com/mayerlyCoro/imagen/assets/164127557/467e4df2-8d5e-4c82-8f66-4faddf898fc9)

##### **COMPROBANTE** ☝
**Objetivo**: de igual manera pero en este caso tenemos dos elementos donde solo nos pide solo seleccionar las cajas bento para ello solo llamamos en nuestra hoja de stilo el nombre de la etigueta bento para asi completr el nivel. 

## ***NIVEL 3***🧠

![3](https://github.com/mayerlyCoro/imagen/assets/164127557/36b1be45-99f9-4df3-beda-bba3a0d8d768)
##### **COMPROBANTE** ☝ 
**Objetivo**: sleecionamos mediente un identificador para que podamos recocnocer a la etiqueta  para poder soluciionar este problema 

## ***NIVEL 4*** 🧠

![4](https://github.com/mayerlyCoro/imagen/assets/164127557/212b5312-89bc-4f90-b97d-69f82c0866dd)

##### **COMPROBANTE** ☝
**Objetivo**: Seleccionamos una manzana dentro de un plato, seleccionanmos a una etiqueta qe esta dentro de otra etiqueta

## ***NIVEL 5*** 🧠

![5](https://github.com/mayerlyCoro/imagen/assets/164127557/f20148af-707f-4b4e-b0bb-67f007838109)

##### **COMPROBANTE** ☝
**Objetivo**: seleccionando el pepino al plato elegante.  continuando con los identificadores ahora la tarea es seleccionar un pepino para ello llamamos a la etiqueta plato que tiene un id=fancy y con ello a l pepino 

## ***NIVEL 6*** 🧠

**Objetivo**: selec cionando las manzanas pequeñas. este caso vamos a seleecionanar las manzanas peuqeñas pero nos indican que estas tienen una clase llamanda class=small , entonces llamamos a la clase para cumplir el objetivo. 

![6a](https://github.com/mayerlyCoro/imagen/assets/164127557/21d9778f-e9e5-4d2c-86bc-5643568b392b)


## ***NIVEL 7*** 🧠
**Objetivo**: seleccionanamos las naranjas pequeñas que tenemos, pero estas esten en una etiqueta llmada ORANGE y esta tine un identificador llamado SMALL dando asi competada el nivel.

![7a](https://github.com/mayerlyCoro/imagen/assets/164127557/6f85c61d-69b9-488f-b3ac-50639a43d243)


## ***NIVEL 8*** 🧠
**Objetivo**: nos pide seleccionanr las pequeñas narajas que estan en los BENTOS, las pequeñas naranjas estan dentro de un div llamada ORANGE estas estende detro de otra div llamada BENTO. 

![8a](https://github.com/mayerlyCoro/imagen/assets/164127557/10b96dd1-59c4-45fb-88a0-63abd650dc8f)


## ***NIVEL 9*** 🧠
 **Objetivo**: seleccionanamos platos y bentos pero estas estan detros de un div cada uno pero tiene  un id igual asi que los llamamos a los dos con una coma. 


 ![9a](https://github.com/mayerlyCoro/imagen/assets/164127557/f6691c83-31c3-446b-b873-59823d82b8f1)



## ***NIVEL 10*** 🧠
**Objetivo**: nos indican que seleccionemos todas las cosas para ello utilizaremos el *

![10a](https://github.com/mayerlyCoro/imagen/assets/164127557/c18af4aa-5866-4b0b-ae05-7597f6bcb0da)


## ***NIVEL 11*** 🧠

**Objetivo**: en este caso selecionaremos todos los elementos dentro de una etiqueta, nos piden seleccioanar las frutas dentro de los platos. 

![11a](https://github.com/mayerlyCoro/imagen/assets/164127557/35d45dd8-be6a-4805-984a-7f03bcec2185)


## ***NIVEL 12*** 🧠
**Objetivo**: Podemos usar el Selector de hermanos adyacentes para elegir todas las manzanas al lado de un plato

![12a](https://github.com/mayerlyCoro/imagen/assets/164127557/d91c7a0f-3d09-4623-bdd1-bfa273c9bf37)

## ***NIVEL 13*** 🧠
**Objetivo**: utilice el selector general de deslizamiento para colocar todos los elementos de pepinillo junto a un bento.

![13a](https://github.com/mayerlyCoro/imagen/assets/164127557/65347da0-8040-4c4a-9174-85c022344445)


## ***NIVEL 14*** 🧠
**Objetivo**:  Usamos el Selector de Niños para elegir la manzana que es hija de un plato.(herencias) 

![14a](https://github.com/mayerlyCoro/imagen/assets/164127557/a620ab80-3407-4b62-b74c-c7a4792a2869)


## ***NIVEL 15*** 🧠
**Objetivo**: Seleccione un primer elemento hijo dentro de otro elemento , para seleccionar el primer hijo de un grupo de niños, utilice el pseudoselector de primer hijo.


![15a](https://github.com/mayerlyCoro/imagen/assets/164127557/2a358305-be19-4d9e-88f6-ca5b76410954)


## ***NIVEL 16*** 🧠
**Objetivo**: 
 Seleccione un elemento que sea el único elemento dentro de otro. 

 ![16a](https://github.com/mayerlyCoro/imagen/assets/164127557/d16562dd-c25e-44ce-a70e-cfd7fb162cc3)


## ***NIVEL 17*** 🧠
**Objetivo**: 
 Puede utilizar este selector para seleccionar un elemento que sea el último elemento secundario dentro de otro elemento.

 ![17a](https://github.com/mayerlyCoro/imagen/assets/164127557/31737be0-69ed-4fee-acd5-bda1fbc5c5a2)


## ***NIVEL 18*** 🧠
**Objetivo**: 
 utilizamos el pseudo-selector de enésimo hijo, “nth-child(3)” y simplemente especificamos que queremos encontrar un elemento, que es el tercer elemento hijo de otro elemento. Si tuviéramos más elementos, con 3 o más hijos, podríamos usar "plate:nth-child(3)".

 ![18a](https://github.com/mayerlyCoro/imagen/assets/164127557/182f703b-b0e6-464a-8d38-c320204d6726)


## ***NIVEL 19*** 🧠
**Objetivo**: 
.Seleccionamos el tercer hijo del final de los elementos con el enésimo último selector de hijos, “:nth-last-child(3)”. Luego decimos que sólo queremos mirar los elementos “bento”.

![19a](https://github.com/mayerlyCoro/imagen/assets/164127557/b0aa8a58-4243-4093-9bcb-5c276f63a275)

## ***NIVEL 20*** 🧠
**Objetivo**: 
 
utilizamos el selector de primero de tipo, selector “:primero de tipo” y luego especificamos que queremos ver manzanas.

![23a](https://github.com/mayerlyCoro/imagen/assets/164127557/603e6881-9daa-4f14-b8c6-020d780cce24)


## ***NIVEL 21*** 🧠
**Objetivo**: 
utilizamos el selector de primero de tipo, selector “:primero de tipo” y luego especificamos que queremos ver manzanas.

![21a](https://github.com/mayerlyCoro/imagen/assets/164127557/aba296ba-e108-4f90-809e-9f4a77c82bc0)


## ***NIVEL 22*** 🧠

![7](https://github.com/mayerlyCoro/imagen/assets/164127557/8bc8fc39-e590-4d94-aa44-d18cc3ef7bf8)

##### **COMPROBANTE** ☝
**Objetivo**: 
 queremos seleccionar cada segundo elemento a partir de (e incluyendo) la tercera instancia. Podríamos hacerlo aún más específico si tuviéramos diferentes elementos con "placa: enésima de tipo (2n+3)".

 ![22a](https://github.com/mayerlyCoro/imagen/assets/164127557/a48f86b6-b834-46f5-b535-f0433a4852ef)


## ***NIVEL 23*** 🧠

![8](https://github.com/mayerlyCoro/imagen/assets/164127557/cb887899-0aef-4437-981e-8c4f0a1b2331)

##### **COMPROBANTE** ☝
**Objetivo**: 
Con el selector Sólo de tipo, “:solo-de-tipo”, seleccionamos el elemento manzana, si es el único de su tipo dentro de su elemento padre. Es posible que desee limitar la selección con "placa de manzana: solo de tipo".

![23a](https://github.com/mayerlyCoro/imagen/assets/164127557/33e43573-1e06-491a-bf81-1ebe989c0af4)

## ***NIVEL 24*** 🧠
![9](https://github.com/mayerlyCoro/imagen/assets/164127557/610f70b3-b9da-4a1f-8fb8-e7c4a829632b)

##### **COMPROBANTE** ☝
**Objetivo**: 
.Usando el último selector de tipo encontramos que el último elemento de la clase es pequeño.

![24a](https://github.com/mayerlyCoro/imagen/assets/164127557/56739db3-2838-4782-bc41-46d478a41306)


## ***NIVEL 25*** 🧠
![10B](https://github.com/mayerlyCoro/imagen/assets/164127557/96480739-2d22-4864-a356-93de71cc75e4)

##### **COMPROBANTE** ☝ 
**Objetivo**: 
Usamos el Selector de vacíos, “:empty” para encontrar todos los bentos vacíos.

![25a](https://github.com/mayerlyCoro/imagen/assets/164127557/f2c0f8b6-1704-46a3-b9d7-cc44a2d3878a)


## ***NIVEL 26*** 🧠

![11B](https://github.com/mayerlyCoro/imagen/assets/164127557/a9e7f9da-3e1d-4794-80ad-934fd5fb5f4b)

##### **COMPROBANTE** ☝ 
**Objetivo**: 
 Ahora es el momento de la pseudoclase de Negación, “:not”. Aquí seleccionamos todas las manzanas que no pertenecen a la clase "pequeña".

 ![26a](https://github.com/mayerlyCoro/imagen/assets/164127557/46310ebb-f3a7-4aea-8e8b-76a29b8ef6b5)


 ## ***NIVEL 27*** 🧠

 ![12B](https://github.com/mayerlyCoro/imagen/assets/164127557/95d3fc88-df62-4380-a715-2450e4f266a4)

 ##### **COMPROBANTE** ☝ 
**Objetivo**: 
Aplicamos el selector de Atributos y seleccionamos todos los elementos con un atributo “for=".

![27a](https://github.com/mayerlyCoro/imagen/assets/164127557/abf86357-7aa2-4b0e-87d9-e24727f7ee44)


## ***NIVEL 28*** 🧠

![13B](https://github.com/mayerlyCoro/imagen/assets/164127557/36871d77-1a6b-4ef0-8e93-395f4501ba1d)

##### **COMPROBANTE** ☝
**Objetivo**: 
Nuevamente usamos el selector de atributos y especificamos que queremos seleccionar todas las placas con un atributo “for=".

![28a](https://github.com/mayerlyCoro/imagen/assets/164127557/dbae5aef-f926-4ada-ae4d-9a95dd514f6b)


## ***NIVEL 29*** 🧠


##### **COMPROBANTE** ☝
**Objetivo**: 
Para seleccionar la comida de Vitaly aplicamos el Selector de Valor de Atributo y buscamos el valor específico, “Vitaly”.

![29a](https://github.com/mayerlyCoro/imagen/assets/164127557/4ade7dd6-989e-46ae-bac2-322098826a7c)

## ***NIVEL 30*** 🧠

![15B](https://github.com/mayerlyCoro/imagen/assets/164127557/b3d92401-0015-4e5e-b968-b8e2e71d3eca)

##### **COMPROBANTE** ☝ 
**Objetivo**: 
 aplique el atributo comienza con el selector y especifique los caracteres, "Sa".

 ![30a](https://github.com/mayerlyCoro/imagen/assets/164127557/59b49d61-e764-4e84-ad9d-5470d45b0e80)


 ## ***NIVEL 31*** 🧠
![6B](https://github.com/mayerlyCoro/imagen/assets/164127557/8d1a3f79-533d-41b2-a1be-59fdf2f0b5bb)
 
 ##### **COMPROBANTE** ☝
**Objetivo**: 
imilar al nivel 30, podemos usar el selector de atributo termina en y buscar el valor del atributo que termina en “ato”.

![31a](https://github.com/mayerlyCoro/imagen/assets/164127557/41151586-d2d8-4c62-a57d-0dbf79f8b291)


## ***NIVEL 32*** 🧠

![8B](https://github.com/mayerlyCoro/imagen/assets/164127557/337fc3f2-aa01-467d-85f2-54fb2569204c)

##### **COMPROBANTE** ☝
**Objetivo**: 
aplique el selector comodín de atributo para el atributo for con el valor "obb".

![32a](https://github.com/mayerlyCoro/imagen/assets/164127557/da05f652-f493-4d3c-9c96-a4ac5175156f)

## ***FINALMENTE*** 🏆 🏆

![Captura de pantalla 2024-04-07 175300](https://github.com/mayerlyCoro/imagen/assets/164127557/d98f94ee-0fff-491c-ac3f-279796dae65d)


![Captura de pantalla 2024-04-07 175315](https://github.com/mayerlyCoro/imagen/assets/164127557/1746a870-4a17-4d8f-b426-eb7ab89c0fcd)

LOGRANDO ASI COMPLETAR LOS NIVELES 