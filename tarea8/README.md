# ***PRÁCTICA 7***  📚  👩🏻‍💻

**-DATOS PERSONALES**
| **NOMBRE  🌻** | **MATERIA  💻**    | **AUXILIAR  🐱**                |
| :-------- | :------- | :------------------------- |
| *Mayerly Coro Canaviri* | *Diseño y Programacion* | *Sergio Apaza*|

```http
```
***TAREAS A COMPLETAR*** 🤓


- [X]  ☝️  Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal.
- [X]  ☝️ Realice un video explicando lo siguiente: 📺
+ La creación de una carpeta en el disco local C:, mediante comandos desde la terminal
+ La creación de un proyecto utilizando React con Next.js
NOTA: El video solo deberá explicar la creación de UN SOLO proyecto
 - [X]  ☝️ ¿Qué comandos se utilizó en las anteriores preguntas? 
- [X]  ☝️ ¿Qué errores tuvo al crear el proyecto? Tome en cuenta los errores que tuvo al crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE ABRIL DE 2024. 😣
IMPORTANTE: De preferencia, adjunte las capturas de dichos errores.
- [X]  ☝️ Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color naranja y no se puede acceder al mismo, tal y como se muestra en la imagen. Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la carpeta aparece vacía.
Averigüe la causa del problema y la solución del mismo. ⁉️


```http
```
***OBJETIVO***  ⚠  📚

+  🔗 Preparar y entregar la práctica en formato digital según lo indicado, utilizando el repositorio designado.  📈

+  🔗 Asegurarse de que todos los archivos estén correctamente nombrados, especialmente el archivo principal que debe llamarse "HW8_guideToReact.md".  📈


+  🔗 Tomar capturas de pantalla que muestren claramente la versión de Node.js y npm, incluyendo la hora, utilizando los comandos adecuados en la terminal.  📈

+  🔗 Adjuntar capturas de pantalla de los errores encontrados durante la creación del proyecto, tanto para esta práctica como para el laboratorio del día martes, 16 de abril de 2024. Documentar los errores y las soluciones aplicadas.  📈


+  🔗 Grabar un video explicando la creación de una carpeta en el disco local C: utilizando comandos de terminal.  📈

+  🔗 Explicar detalladamente la creación de un proyecto utilizando React con Next.js. Asegurarse de seguir los pasos proporcionados y explicar cada comando utilizado.  📈


+  🔗 Identificar y documentar los comandos utilizados en cada paso de las preguntas anteriores.  📈

+  🔗 Investigar y proporcionar una explicación sobre el problema de las carpetas que se tornan de color naranja en el repositorio, así como ofrecer una solución efectiva.  📈


+  🔗 Organizar la entrega de la tarea de manera ordenada y profesional, asegurándose de que la documentación esté claramente estructurada y sea fácil de entender.  📈


+  🔗Asegurarse de comprender completamente cada paso del proceso de creación de la carpeta y del proyecto React con Next.js, así como los comandos utilizados. Esto ayudará a explicar con claridad en el video y a resolver problemas de manera efectiva.  📈
```http

```
## ***PREGUNTA 1*** 👩🏻‍💻 🌻
- [X]     🌟 Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal. 

+ Capturita  🙈

![Captura de pantalla 2024-04-23 223512](https://github.com/mayerlyCoro/imagen/assets/164127557/42f99380-5e56-41b6-ae81-5bd0b8bb5dab)


```http

```
 

## ***PREGUNTA 2*** 👩🏻‍💻 🌻

- [X]    📘Realice un video explicando lo siguiente:  👐
+ La creación de una carpeta en el disco local C:, mediante comandos desde la terminal
+ La creación de un proyecto utilizando React con Next.js

+ Ahora a diferencia de la anterior PRACTICA lo subimos al YOU TUBE, ahora si como influencers  😎 😈  🤘

⚠️***Aqui el videito, presione la Lupita***     [  🔎 ](https://youtu.be/e5mxfWDZm_I)




```http

```
 

## ***PREGUNTA 3*** 👩🏻‍💻 🌻

- [x]   Que comandos se utilizó en las anteriores preguntas.❓❓

***LOS COMANDOS UNTILIZADOS***  😌

 🔴 **NODE --VERSION** : Este comando se utiliza para mostrar la versión de Node.js instalada en tu sistema. Al ejecutar este comando en la terminal, te devolverá la versión actual de Node.js que está instalada. 

 🔴 **NPM --VERSION** : Similar al comando anterior, este se utiliza para mostrar la versión de npm (Node Package Manager) instalada en tu sistema. 

 🔴 **COMANDO npx create-next-app@latest** : se utiliza para crear un nuevo proyecto de Next.js utilizando la plantilla más reciente disponible. 

 🔴 **CD**  : se utiliza para cambiar el directorio actual en el símbolo del sistema (cmd).

 🔴 **CODE NOMBRE**: para que se puede abrir VISUAL

 🔴 **NPM RUM DEV**: Este comando se utiliza para ejecutar un script llamado "dev" definido en el archivo package.json de tu proyecto. 

```http

```
 

## ***PREGUNTA 4*** 👩🏻‍💻 🌻
- [x] ¿Qué errores tuvo  😿 al crear el proyecto? Tome en cuenta los errores que tuvo al crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE ABRIL DE 2024.


***ERRORES***  😪 💀

❎ ERROR 1  💀: me puse muy nerviosa y eso influencio en que me pueda decenvolver al momento de crear mi proyecto 

❎ ERRO 2  💀: me complique en los comandos y uno de ellos fue que no puede crear mi proyecto en el local C 

❎ ERROR 3  💀: los permisos, no me paso pero es bueno recalcarlo asi tambien como los antivirus, 

❎  ERROR 4  💀: errores de sintaxis al momento de escribir los comandos

❎ ERROR 5  💀:  surgio errores si en el entorno de desarrollo que no está configurado correctamente. Esto influencio en  configuraciones de ruta incorrectas, problemas de variables de entorno y tambien en  configuraciones incompatibles entre distintas herramientas.


```http

```
 

## ***PREGUNTA 5*** 👩🏻‍💻 🌻

- [x]  Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color naranja y no se puede acceder al mismo, tal y como se muestra en la imagen. Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la carpeta aparece vacía. Averigüe la causa del problema y la solución del mismo.


**PODRIA SER POR :**  🚩

⛔ Ignorando Archivos , a veces, tenemos archivos que no queremos incluir en nuestro repositorio, como archivos temporales o generados automáticamente. Estos archivos pueden estar especificados en un archivo especial llamado .gitignore. Si la carpeta se vuelve naranja, podría ser porque los archivos dentro de ella están siendo ignorados por Git.  😰

⛔ Los permisos son como reglas que controlan quién puede hacer qué con un archivo o carpeta. Si los permisos no están configurados correctamente, Git puede tener problemas para acceder a los archivos dentro de la carpeta, lo que causa que aparezca naranja.  😨

⛔ A veces, los cambios que hacemos en nuestro repositorio local no se sincronizan correctamente con el repositorio en GitLab. Esto puede causar problemas de visualización, como una carpeta que se vuelve naranja.  😒

**La solución a este problema puede variar dependiendo de la causa específica, pero aquí hay algunas cosas que podrías intentar:**  🙌

 🔲Verificar si los archivos dentro de la carpeta deben estar siendo ignorados por Git y ajustar la configuración en el archivo .gitignore si es necesario. 😉

 🔲Aseguranos de que los permisos de los archivos y carpetas dentro de la carpeta sean correctos y permitan a Git acceder a ellos. 😜

 🔲Intentar sincronizar nuevamente tu repositorio local con GitLab para asegurarte de que estén alineados correctamente. 😃



****TAREA COMPLETADA**** 🎉